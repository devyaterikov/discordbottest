const http = require('http')
const WebSocket = require('ws')
const fs = require('fs')
const path = require('path')
const request = require('request')

const optionsDiscordQuery = {
    'method': 'POST',
    'url': 'https://discord.com/api/channels/756115666808864808/messages',
    'headers': {
      'Authorization': 'Bot NzU1NDQ4MTMyNDUxNDM0NjE3.X2Db5g.LAOd0Kab28F4kuFrQHWhAnbmIt0',
      'Content-Type': 'application/json',
      'Cookie': '__cfduid=dd1d6b3f25d5c598e21a2981bd344259e1600254789'
    },
    'body': ''
};

const optionsCatsQuery = {
    'method': 'GET',
    'url': 'https://api.thecatapi.com/v1/images/search'
};

function sendCats() {
    request(optionsCatsQuery, function (error, response) {
        if (error) throw new Error(error);
        const resQuery = JSON.parse(response.body)
        optionsDiscordQuery.body = JSON.stringify({
            "content": resQuery[0].url
        })
        request(optionsDiscordQuery, function (error, response) {
            if (error) throw new Error(error);
            console.log(response.body);
        })
    })
}

const socket = new WebSocket("wss://gateway.discord.gg/?v=6&encoding=json");
let startedHeartBeat = false;

function startingHeartBeat(interval) {
    if (!startedHeartBeat) {
        setInterval(() => {
            socket.send('{"op": 1,"d": 251}')
        }, interval)
        startedHeartBeat = true
    }
}

function identifying() {
    socket.send(JSON.stringify({
        "op": 2,
        "d": {
          "token": "NzU1NDQ4MTMyNDUxNDM0NjE3.X2Db5g.LAOd0Kab28F4kuFrQHWhAnbmIt0",
          "properties": {
            "$os": "windows",
            "$browser": "rock's_bot",
            "$device": "rock's_bot"
          }
        }
    }))
}

socket.onopen = function() {
    console.log("[open]");
};

socket.onmessage = function(event) {
    const msg = JSON.parse(event.data)
    //console.log(`[message]: ${event.data}`)
    if (msg.op != 10 && msg.op != 11) {
        console.log('[message]:')
        console.log(msg)
    }
    if (msg.op == 0 && msg.t == 'MESSAGE_CREATE') {
        let txt = msg.d.content
        if (txt == '<@!755448132451434617> -getCats') sendCats()
    }
    if (msg.op == 10) {
        startingHeartBeat(msg.d.heartbeat_interval)
        identifying()
    }
};

socket.onclose = function(event) {
    console.log(`[close]: clean_closing: ${event.wasClean}, with ${event.code} code`)
}

const server = http.createServer((req, res) => {
    if (req.method === 'GET') {
        res.writeHead(200, {
            'Content-Type': 'text/html charset=utf-8'
        })
        if (req.url === '/') {
            fs.readFile(path.join(__dirname, 'views', 'index.html'), 'utf-8', (err, content) => {
                if (err) throw err
                res.end(content)
            })
        }
    }
    if (req.method === 'POST') {
        if (req.url === '/cats') {
            sendCats()
            fs.readFile(path.join(__dirname, 'views', 'index.html'), 'utf-8', (err, content) => {
                if (err) throw err
                res.end(content)
            })
        }
        if (req.url === '/message') {
            const body = []
            req.on('data', data => {
                body.push(Buffer.from(data))
            })
            req.on('end', () => {
                optionsDiscordQuery.body = JSON.stringify({
                    'content': JSON.parse(body.toString()).content,
                    'tts': false
                })
                request(optionsDiscordQuery, function (error, response) {
                    if (error) throw new Error(error);
                    console.log(response.body);
                })
            })
            fs.readFile(path.join(__dirname, 'views', 'index.html'), 'utf-8', (err, content) => {
                if (err) throw err
                res.end(content)
            })
        }
    }
})

server.listen(3000, () => {
    console.log('Server is running...')
})